\documentclass{beamer}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumerate}
\usepackage{cases}
\usepackage{graphicx}
\usepackage[mathscr]{euscript}

\definecolor{forestgreen}{HTML}{009900}
\definecolor{brown}{HTML}{996600}

\newcommand\red[1]{\textcolor{red}{#1}}
\newcommand\green[1]{\textcolor{forestgreen}{#1}}
\newcommand\blue[1]{\textcolor{blue}{#1}}
\newcommand\brown[1]{\textcolor{brown}{#1}}

\title[QAP] % (optional, only for long titles)
{An Application of QAP to Code Layout on Multicore Processors}
%\subtitle{}
\author[Author, Anders] % (optional, for multiple authors)
{Phitchaya Mangpo Phothilimthana \\ \and Nishant Totla \\ \and Sarah Chasins}
\institute[Cal] % (optional)
{
  University of California, Berkeley
}
\date[2013] % (optional)
{CS270, Spring 2013}

\begin{document}
\frame{\titlepage}

\begin{frame}
  \frametitle{Motivation}
  %\framesubtitle{A bit more information about this}
  % TODO
  \begin{itemize}
  \item GreenArray
    \begin{itemize}
    \item Low power multicore architecture.
    \item $8 \times 18$ grid of tiny cores.
    \item Very small programs fit in single core.  Others must be partitioned.
    \item Difficult to program.
      \begin{itemize}
        \item Partition program into smaller components.
        \item Write message sending code to facilitate communication between partitions, over ports.
        \item Lay out partitions on grid to minimize requisite message sends.
      \end{itemize}
    \end{itemize}  
  \item Creating new programming system for hard-to-use, many-core architectures like GreenArray
    \begin{itemize}
    \item Input: a program as it would be written for a standard architecture
      \begin{itemize}
      \item No partitions, communication code, or specified layout
      \end{itemize}
    \item Output: a partitioned program, an assignment of partitions to cores, partitions augmented with communication code
      \begin{itemize}
      \item Ready to run
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Motivation}
  %\framesubtitle{A bit more information about this}
  % TODO
  \begin{itemize}
  \item Partition Location Problem
    \begin{itemize}
    \item One component of our the programming system
      \begin{itemize}
      \item Assume partitions are given
      \end{itemize}
    \item Assign $n$ code partitions to $n$ cores
    \item Between any partition pair $p_1 \neq p_2$, may be 0 or more messages
      \begin{itemize}
      \item Data flow graph
      \end{itemize}
    \item Between any core pair $c_1 \neq c_2$, takes 1 or more port reads to pass a message
      \begin{itemize}
      \item Manhattan distance yields shortest distance graph
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Introduction to QAP}
  %\framesubtitle{A bit more information about this}
  %More content goes here
  \begin{itemize}
  \item Quadratic Assignment Problem
    \begin{itemize}
    \item Definition
      \begin{itemize}
      \item Set $F$, facilities
      \item Set $L$, locations
      \item Traffic function $t : F \times F \rightarrow \mathbb{R} $
      \item Distance function $d : L \times L \rightarrow \mathbb{R} $
      \item Find assignment $a: F \rightarrow L$ to minimize cost function:
      
      $\displaystyle\sum\limits_{f_1 \in F, f_2 \in F} t(f_1,f_2) \cdot d(a(f_1),a(f_2))$ 
      
      \end{itemize}
      
    \item Intuitively: locate factories close together if there needs to be substantial traffic (flow) between them
    
    \item Extremely difficult optimization problem
      \begin{itemize}
      \item Generalization of TSP
      \end{itemize}
    \item Some special cases are polynomial time solvable
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Partition Location as QAP Instance}

      \begin{itemize}
      \item Set $F$, partitions
      \item Set $L$, cores
      \item Traffic function $t : F \times F \rightarrow \mathbb{R} $, data flow between partitions
      \item Distance function $d : L \times L \rightarrow \mathbb{R} $, shortest distance between cores
      \item Find assignment $a: F \rightarrow L$ to minimize cost function:
      
      $\displaystyle\sum\limits_{f_1 \in F, f_2 \in F} t(f_1,f_2) \cdot d(a(f_1),a(f_2))$ 
      
      Total number of port reads during program execution
      
      \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Problem Formulation}
  \begin{align*}
    \min \sum_{i \in N} \sum_{k \in N_i} d_{ik}f_{\pi(i)\pi(k)} \\
    s.t. \quad \pi \in \mathscr{P}_n
  \end{align*}
  \begin{itemize}
    \item $D$ - distance matrix
    \item $F$ - flow matrix
    \item $\mathscr{P}_n$ - set of permutations of $[1,2,3,...,n]$
    \item $N = \{1,2,...,n\}$
    \item $N_i = N \backslash \{i\}$
  \end{itemize}
  %% \begin{block}{Block title}
  %% \end{block}
  %% \begin{alertblock}{Block title}
  %% \end{alertblock}
  %% \begin{exampleblock}{Block title}
  %% \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Problem Formulation}
  \begin{align*}
    \min \sum_{i \in N} \sum_{k \in N_i} d_{ik}f_{\pi(i)\pi(k)} \\
    s.t. \quad \pi \in \mathscr{P}_n
  \end{align*}
    \begin{center}
    \begin{tabular}{cccc}
      $D = \left| \begin{array}{ccc}
        0 & 1 & 2\\
        1 & 0 & 1\\
        2 & 1 & 0 \end{array} \right|$
      &
      \includegraphics[scale=0.2]{grid_empty}
      &
      $F = \left| \begin{array}{ccc}
        0 & 5 & 10\\
        5 & 0 & 1\\
        10 & 1 & 0 \end{array} \right|$ 
      &
      \includegraphics[scale=0.4]{flow}
    \end{tabular}
    \end{center}
  $\pi = [1,2,3] \Rightarrow cost = 2 \times (5 + 1 + 20) = 52$ 
  \includegraphics[scale=0.4]{grid_map1}
  \\
  $\pi = [2,1,3] \Rightarrow cost = 2 \times (5 + 10 + 2) = 34$
  \includegraphics[scale=0.4]{grid_map2}
\end{frame}

\begin{frame}
  \frametitle{Another Representation -- $\mathbb{M}^n$ to $\mathbb{M}^{n^2}$ Form}
{\footnotesize
  \[
    \min \sum_{i \in N} \sum_{k \in N_i} d_{ik}f_{\pi(i)\pi(k)} \Rightarrow
    \min \sum_{i \in N} \sum_{k \in N_i} q_{i \pi(i) k \pi(k)}
  \]

  \begin{center}
  \begin{tabular}{cc}
    \red{$i \backslash k$} & \\
    $D =$ & 
    $\left| \begin{array}{ccc}
      0 & 1 & 2\\
      1 & 0 & 1\\
      2 & 1 & 0 \end{array} \right| $
  \end{tabular}
  \begin{tabular}{cc}
    \green{$j \backslash l$} & \\
    $F =$ & 
    $\left| \begin{array}{ccc}
      0 & 5 & 10\\
      5 & 0 & 1\\
      10 & 1 & 0 \end{array} \right| $
  \end{tabular}
  \end{center}

    \begin{tabular}{cc}
      $q_{ijkl} = d_{ik} \times f_{jl}$,
      &
      \begin{tabular}{ccc}
        $\red{i} \backslash \green{j}$ & & \\
        & {\tiny $\red{k} \backslash \green{l}$} & \\
        $Q =$ & & 
        $
      \begin{tabular}{ccc|ccc|ccc}
        0 & 0 & 0    &  0 & 0 & 0   &  0 & 5 & 10 \\
        0 & 5 & 10   &  5 & 0 & 1   &  0 & 0 & 0  \\
        0 & 10 & 20  &  10 & 0 & 2  &  0 & 5 & 10 \\
        \hline
        0 & 5 & 10   &  5 & 0 & 1   & 10 & 1 & 0 \\
        0 & 0 & 0    &  0 & 0 & 0   & 0 & 0 & 0  \\
        0 & 5 & 10   &  5 & 0 & 1   & 10 & 1 & 0 \\
        \hline
        0 & 10 & 20  &  10 & 0 & 2  & 20 & 2 & 0 \\
        0 & 5 & 10   &  5 & 0 & 1   & 10 & 1 & 0 \\
        0 & 0 & 0    &  0 & 0 & 0   & 0 & 0 & 0
      \end{tabular}$
      \end{tabular}
    \end{tabular}
}

\end{frame}

\begin{frame}
  \frametitle{Another Representation}
  \[\pi = [2,1,3] \Rightarrow cost = 2 \times (5 + 10 + 2) = \red{34}\]
  \begin{tabular}{ccc}
    $i \backslash \pi(i)$ & & \\
    & {\footnotesize $k \backslash \pi(k)$} & \\
    $Q =$ & &
    $\begin{tabular}{ccc|ccc|ccc}
      0 & 0 & 0    &  \brown{0} & \red{0} & \brown{0}   &  0 & 5 & 10 \\
      0 & 5 & 10   &  \red{5} & \brown{0} & \brown{1}   &  0 & 0 & 0  \\
      0 & 10 & 20  &  \brown{10} & \brown{0} & \red{2}  &  0 & 5 & 10 \\
      \hline
      \brown{0} & \red{5} & \brown{10}   &  5 & 0 & 1   & 10 & 1 & 0 \\
      \red{0} & \brown{0} & \brown{0}    &  0 & 0 & 0   & 0 & 0 & 0  \\
      \brown{0} & \brown{5} & \red{10}   &  5 & 0 & 1   & 10 & 1 & 0 \\
      \hline
      0 & 10 & 20  &  10 & 0 & 2  & \brown{20} & \red{2} & \brown{0} \\
      0 & 5 & 10   &  5 & 0 & 1   & \red{10} & \brown{1} & \brown{0} \\
      0 & 0 & 0    &  0 & 0 & 0   & \brown{0} & \brown{0} & \red{0}
    \end{tabular}$
  \end{tabular}

\end{frame}

\begin{frame}
  \frametitle{Approaches}
  \begin{itemize}
    \item Linearization
    \item QAP Exact Approaches
    \item QAP Approximation Approaches
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Linearization Approach}
  \begin{itemize}
    \item Kabadi and Punnen
  \end{itemize}
  \textbf{Theorem 1} For any $Q \in \mathbb{M}^{n^2}$ there exists a matrix $Q^R \in \mathbb{M}^{n^2}$ such that $Q^R$ is in \green{\emph{quadratic reduced form}}, and \green{\emph{QP-equivalent}} to Q. \cite{Punnen}

  \begin{block}{Quadratic Reduced Form}
    All elements in row $(i,j)$ and column $(k,l)$ where $(i,j)$ and $(k,j) \in \{(n,p), (p,n): p \in N\}$ are zeros, \\
except possibly $q_{npnp}^R$ and $q_{pnpn}^R, p \in N$.
  \end{block}

  \begin{block}{QP-equivalent and QP-constant}
    $Q^1$ and $Q^2$ are \emph{quadratic permutation equivalent (QP-equivalent)} if $Q^1[\pi] = Q^2[pi], \forall \pi \in \mathscr{P}_n$. \\
    $Q$ is \emph{QP-constant} if $\exists K, Q[\pi] = K, \forall \pi \in \mathscr{P}_n$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Linearization Approach}
  Original matrix
  \[Q = 
      \begin{tabular}{ccc|ccc|ccc}
        0 & 0 & 0    &  0 & 0 & 0   &  0 & 5 & 10 \\
        0 & 5 & 10   &  5 & 0 & 1   &  0 & 0 & 0  \\
        0 & 10 & 20  &  10 & 0 & 2  &  0 & 5 & 10 \\
        \hline
        0 & 5 & 10   &  5 & 0 & 1   & 10 & 1 & 0 \\
        0 & 0 & 0    &  0 & 0 & 0   & 0 & 0 & 0  \\
        0 & 5 & 10   &  5 & 0 & 1   & 10 & 1 & 0 \\
        \hline
        0 & 10 & 20  &  10 & 0 & 2  & 20 & 2 & 0 \\
        0 & 5 & 10   &  5 & 0 & 1   & 10 & 1 & 0 \\
        0 & 0 & 0    &  0 & 0 & 0   & 0 & 0 & 0
      \end{tabular}\]

\end{frame}

\begin{frame}
  \frametitle{Linearization Approach}
  Quadratic reduced form
  \[Q^R = 
  \begin{tabular}{ccc|ccc|ccc}
    28 & 0 & 0   &  0 & 1 & 0   &  0 & 0 & 22 \\
    0 & 12 & 0   &  12 & 0 & 0  &  0 & 0 & 0  \\
    0 & 0 & 0    &  0 & 0 & 0   &  0 & 0 & 0 \\
    \hline
    0 & 12 & 0   &  12 & 0 & 0  &  0 & 0 & 0 \\
    9 & 0 & 0    &  0 & 0 & 0   &  0 & 0 & 11  \\
    0 & 0 & 0    &  0 & 0 & 0   &  0 & 0 & 0 \\
    \hline
    0 & 0 & 0    &  0 & 0 & 0   &  0 & 0 & 0 \\
    0 & 0 & 0    &  0 & 0 & 0   &  0 & 0 & 0 \\
    30 & 0 & 0   &  0 & 3 & 0   &  0 & 0 & 0
  \end{tabular}\]

\end{frame}

\begin{frame}
  \frametitle{Linearization Approach}
  Quadratic reduced form
  \[Q^R = 
  \begin{tabular}{ccc|ccc|ccc}
    28 & 0 & \red{0}               &  0 & 1 & \red{0}             &  \red{0} & \red{0} & \green{22} \\
    0 & 12 & \red{0}               &  12 & 0 & \red{0}            &  \red{0} & \red{0} & \red{0} \\
    \red{0} & \red{0} & \red{0}    &  \red{0} & \red{0} & \red{0} &  \red{0} & \red{0} & \red{0} \\
    \hline
    0 & 12 & \red{0}   &  12 & 0 & \red{0}                          &  \red{0} & \red{0} & \red{0} \\
    9 & 0 & \red{0}    &  0 & 0 & \red{0}                           &  \red{0} & \red{0} & \green{11}  \\
    \red{0} & \red{0} & \red{0}    &  \red{0} & \red{0} & \red{0}   &  \red{0} & \red{0} & \red{0} \\
    \hline
    \red{0} & \red{0} & \red{0}  &  \red{0} & \red{0} & \red{0}  & \red{0} & \red{0} & \red{0} \\
    \red{0} & \red{0} & \red{0}  &  \red{0} & \red{0} & \red{0}  & \red{0} & \red{0} & \red{0} \\
    \green{30} & \red{0} & \red{0}  &  \red{0} & \green{3} & \red{0}  & \red{0} & \red{0} & \green{0}
  \end{tabular}\]

\end{frame}

\begin{frame}
  \frametitle{Linearization Approach}
  Delete \emph{redundant} rows and columns.
  \[
  \bar{Q} = 
  \begin{tabular}{cc|cc}
    28 & 0   &  0 & 1  \\
    0 & 12   &  12 & 0  \\
    \hline
    0 & 12   &  12 & 0 \\
    9 & 0    &  0 & 0
  \end{tabular}
  \]
  \pause
  Set all diagonal elements to zero.
  \[
  Q' =
  \begin{tabular}{cc|cc}
    \blue{0} & 0    &  0 & \blue{0}  \\
    0 & 12   &  12 & 0  \\
    \hline
    0 & 12   &  12 & 0 \\
    \blue{0} & 0    &  0 & \blue{0}
  \end{tabular}
  \]

\end{frame}

\begin{frame}
  \frametitle{Linearization Approach}
  \textbf{Theorem 2} Let $Q^R \in \mathbb{M}^{n^2}$ be in quadratic reduced form. Then $Q^R$ is linearizable if and only if the following two conditions hold \cite{Punnen}:
  \begin{enumerate}[(i)]
  \item For all $(i,j) \in N_n \times N_n$, the submatrix of \green{$Z^{ij}$} obtained by deleting its $i^{th}$ row and $j^{th}$ column is an LP-constant matrix.
  \item \green{$Q'$} is a QP-constant matrix.
  \end{enumerate}

  \[
  Z^{ij} \in \mathbb{M}^{n-1}, \quad
  z_{uv}^{ij} =
  \begin{cases}
    \bar{q}_{ijuv} & \text{if } (i,j) = (u,v),\\
    \bar{q}_{ijuv} + \bar{q}_{uvij} & \text{if } (i,j) \neq (u,v).
  \end{cases}
  \]
\end{frame}

\begin{frame}
  \frametitle{Linearization Approach -- Counterexample}
  \begin{center}
    \begin{tabular}{cc}
      $D = \left| \begin{array}{cccc}
        0 & 1 & 1 & 2\\
        1 & 0 & 2 & 1\\
        1 & 2 & 0 & 1\\
        2 & 1 & 1 & 0 \end{array} \right|$
      &
      $F = \left| \begin{array}{cccc}
        0 & 1 & 1 & 2\\
        1 & 0 & 1 & 1\\
        1 & 1 & 0 & 1\\
        2 & 1 & 1 & 0 \end{array} \right|$
    \end{tabular}
  \end{center}

  This instance fails both (i) the LP-constant condition and (ii) the QP-constant condition.
\end{frame}

\begin{frame}
	\frametitle{Exact approach - Branch and bound}
	Searching in a tree, with pruning.
	\pause
	\begin{enumerate}
	\item Start with some initial best known solution
	\item Produce partial solutions [branch step]
	\item For each partial solution, compute lower bound
	\item If bound $>$ best known cost, skip that branch [bound step]
	\item Go back to 2
	\end{enumerate}
	\pause
	The efficiency of branch and bound depends on the following
	\begin{itemize}
	\item Initial solution - the lower, the better
	\item Good bounds - the higher, the better
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Testing Different Bounds}
	\begin{figure}[p]
	\centering
	\includegraphics[width=0.7\textwidth]{graph2.pdf}
	\caption{Nodes explored vs problem size}
	\label{fig:graph2}
	\end{figure}
	The Gilmore-Lawler bound is one of the best known lower bounds for QAP \cite{Li94lowerbounds}.
\end{frame}

\begin{frame}
	\frametitle{Testing Different Bounds}
	\begin{figure}[p]
	\centering
	\includegraphics[width=0.8\textwidth]{graph1.pdf}
	\caption{Time taken vs problem size}
	\label{fig:graph1}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Testing Different Initial Heuristics}
	\begin{figure}[p]
	\centering
	\includegraphics[width=0.7\textwidth]{graph3.pdf}
	\caption{Nodes explored vs starting heuristic (for Gilmore-Lawler bound)}
	\label{fig:graph3}
	\end{figure}
	Initial heuristics help a little, but times are roughly constant.

\end{frame}


\begin{frame}
	\frametitle{Scaling Branch and Bound}
	\begin{table}[h!]
\begin{center}
    \begin{tabular}{|c|c|c|c|c|} \hline
    $n$      & $8$ & $16$   & $36$   & $144$     \\ \hline
    Time(s)  & 0.14  & 2086.5 & Timeout  & Timeout      \\ \hline
    Cost  & 126  & 1610 & 600 (best found)  & N/A     \\ \hline
    Nodes visited  & 859  & 33742824 & N/A  & N/A     \\ \hline
    \end{tabular}
\end{center}
\caption{Performance of branch and bound using the Gilmore-Lawler bound on problems ranging from size 8 to size 144}
\label{table:branch_bound}
\end{table}
Clearly this approach does not scale to the sizes we desire.
\end{frame}

\begin{frame}
  \frametitle{Approximation}
  \begin{itemize}
  \item Why?
    \begin{itemize}
    \item QAP with $n > 20$ intractable with exact approaches
    \item For GreenArray, $n = 144$
    \end{itemize}
  \item Approaches
    \begin{itemize}
    \item Simulated Annealing
    \item Ant Systems
    \item Tabu Search
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Approximation}
\begin{itemize}
\item Simulated Annealing
  \begin{itemize}
  \item Temperature Schedule - control exploration vs. exploitation
  \item First adapted to QAP by Burkard et al. \cite{Burkard}
  \item Neighbors usually defined by location swap \cite{Connolly}
  \item Variations to tailor for QAP:
    \begin{itemize}
    \item Temperature schedule \cite{Misevicius}
    \item Order in which neighbors picked \cite{Connolly}
    \end{itemize}
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Approximation}
\begin{itemize}
\item Ant Systems
  \begin{itemize}
  \item Artificial ants produce solutions, exchange information according to communication scheme
    \begin{itemize}
    \item Analogy to ants seeking food, leaving trails
    \item Choice of next node influenced by other ants' information \cite{Dorigo}
    \end{itemize}
  \item For QAP, exchange information about desirability of location-facility pairing \cite{Gambardella}
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Approximation}
\begin{itemize}
\item Tabu Search
  \begin{itemize}
  \item After finding local optimum by hill climbing, prevent return to local optimum with list of forbidden (taboo) moves
  \item Adapted to QAP by Skorin-Kapov \cite{Skorin-Kapov1} \cite{Skorin-Kapov2}
  \item Neighborhood typically defined by swapping locations
    \begin{itemize}
    \item Tabu list typically pairs of facilities that may not be swapped
    \end{itemize}
  \item Tabu list (size $s$) for QAP may be:
    \begin{itemize}
    \item Items swapped in previous $s$ steps \cite{Skorin-Kapov1}
    \item Swaps that assign both to locations they have had in previous $s$ steps \cite{TaillardRobust}
    \end{itemize}
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Evaluating Application to Partition Location Problem}

    \begin{itemize}
    \item Simulated Annealing
      \begin{itemize}
      \item Connolly's Simulated Annealing for QAP \cite{Connolly}
      \end{itemize}
    \item Ant Systems
      \begin{itemize}
      \item Taillard's FANT (Fast Ant System) \cite{TaillardFANT}
      \end{itemize}
    \item Tabu Search
      \begin{itemize}
      \item Taillard's Robust Tabu for QAP \cite{TaillardRobust}
      \end{itemize}
    \end{itemize}

\end{frame}

\begin{frame}
\frametitle{Scaling Approximation Approaches}

\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|l|l|l|} \hline
    Time (s)      & $2 \times 4$    & $4 \times 9$   & $8 \times 18$     \\ \hline
    SA  & 4.924  & 13.215   & 52.439      \\ \hline
    FANT  & .041  & 1.917   & 156.515     \\ \hline
    Tabu & 2.599 & 66.970 & 1163.683 \\ \hline
    \end{tabular}
\end{center}
\caption{The mean running time (in seconds) over three trials for the three approximation algorithms, on partition location problems ranging from a size of 8 partitions to a size of 144 partitions.}
\label{table:time}
\end{table}


\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|l|l|l|} \hline
    Cost     & $2 \times 4$ & $4 \times 9$ & $8 \times 18$ \\ \hline
    SA    & 126   & 578   & 3306   \\ \hline
    FANT  & 126   & 628   & 3744   \\ \hline
    Tabu & 126   & 588   & 3768   \\ \hline
    \end{tabular}
\end{center}
\caption{The cost of the best solution found by the three approximation algorithms, on partition location problems ranging from a size of 8 partitions to a size of 144 partitions.}
\label{table:cost}
\end{table}
\end{frame}

\begin{frame}
\frametitle{Real Programs with Approximation Approaches}

\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|l|l|} \hline
    Time (s)      & ssd    & md5      \\ \hline
    SA  & 53.429  & 53.685         \\ \hline
    FANT  & 71.104  & 69.494       \\ \hline
    Tabu & 1173.340 & 1181.029  \\ \hline
    \end{tabular}
\end{center}
\caption{The mean running time (in seconds) over three trials for the three approximation algorithms, on two real partition location problems.}
\label{table:timeReal}
\end{table}


\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|l|l|l|} \hline
    Cost     & ssd    & md5      \\ \hline
    SA    & 198   & 1668   \\ \hline
    FANT  & 198   & 1668  \\ \hline
    Tabu & 198   & 1668  \\ \hline
    \end{tabular}
\end{center}
\caption{The cost of the best solution found by the three approximation algorithms, on two real partition location problems.}
\label{table:costReal}
\end{table}

\end{frame}

\begin{frame}[allowframebreaks]{References}
\fontsize{8pt}{7.2}\selectfont
\bibliographystyle{plain}
\bibliography{slides}
\end{frame}

\end{document}
