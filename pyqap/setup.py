################################################################################
#       Copyright (C) 2010 Michael Yurko <myurko@gmail.com>
#
#This file is part of pyQAP.
#
#pyQAP is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 2 of the License, or
#(at your option) any later version.
#
#pyQAP is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with pyQAP.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from os.path import realpath, dirname
from os import system
import numpy
includes = [numpy.get_include()]

setup(
    name='pyQAP', 
    packages = ['pyQAP'],
    version = "Development Branch",
    author = "Michael Yurko",
    author_email = "myurko@gmail.com",
    maintainer = "Michael Yurko",
    maintainer_email = "myurko@gmail.com",
    url = "http://pyqap.googlecode.com",
    description = "A branch and bound solver framework for the Quadratic "+\
                  "Assignment Problem",
    download_url = "http://code.google.com/p/pyqap/downloads/list",
    platforms = ["Linux", "pyQAP may work on other platforms, but is untested"],
    license = "GPL v2+",
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("pyQAP.classes", ["pyQAP/classes.pyx"],
                              include_dirs=includes),
                   Extension("pyQAP.cost", ["pyQAP/cost.pyx"],
                              include_dirs=includes),
                   Extension("pyQAP.bound_helpers",
                            ["pyQAP/bound_helpers.pyx"],
                            include_dirs=includes),
                   Extension("pyQAP.lap", ["pyQAP/lap.pyx"],
                              include_dirs=includes)],
    py_modules = ["pyQAP.bounds", "pyQAP.doctests", "pyQAP.heuristics",
                  "pyQAP.heuristic_helpers", "pyQAP.problems", "pyQAP.solver",
                  "pyQAP.vars"]
)

print
print "Done"


