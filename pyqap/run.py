print "Initializing..."
import pyximport
pyximport.install()
from pyQAP import *

print "Parsing problem..."
prob = problems.QAPLIB('4_9_input_matrix')
# prob = problems.reduced(prob,12)
print "Solving..."
print solver.solve(prob, bound=bounds.gilmore_lawler, heuristic=heuristics.parallel_annealing, verbose = True)
