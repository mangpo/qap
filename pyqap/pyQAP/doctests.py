################################################################################
#       Copyright (C) 2010 Michael Yurko <myurko@gmail.com>
#
#This file is part of pyQAP.
#
#pyQAP is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 2 of the License, or
#(at your option) any later version.
#
#pyQAP is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with pyQAP.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

import classes, solver, bound_helpers, bounds, heuristic_helpers, heuristics
import lap, problems
import doctest
import inspect

def run_all_doctests(verbose = False):
    #run doctests
    print "testing classes ..."
    doctest.testmod(classes, optionflags=doctest.ELLIPSIS, verbose=verbose)
    print "classes tested"
    print "testing solver ..."
    doctest.testmod(solver, optionflags=doctest.ELLIPSIS, verbose=verbose)
    print "solver done"
    print "testing bound_helpers ..."
    doctest.testmod(bound_helpers, optionflags=doctest.ELLIPSIS,
                    verbose=verbose)
    print "bound_helpers done"
    print "testing bounds ..."
    doctest.testmod(bounds, optionflags=doctest.ELLIPSIS, verbose=verbose)
    print "bounds done"
    print "testing heuristic_helpers ..."
    doctest.testmod(heuristic_helpers, optionflags=doctest.ELLIPSIS,
                    verbose=verbose)
    print "heuristic_helpers done"
    print "testing heuristics ..."
    doctest.testmod(heuristics, optionflags=doctest.ELLIPSIS, verbose=verbose)
    print "heuristics done"
    print "testing lap ..."
    doctest.testmod(lap, optionflags=doctest.ELLIPSIS, verbose=verbose)
    print "lap done"
    print "testing problems ..."
    doctest.testmod(problems, optionflags=doctest.ELLIPSIS, verbose=verbose)
    print "problems done"
    print "all done"
    
if __name__ == "__main__":
    run_all_doctests()
