################################################################################
#       Copyright (C) 2010 Michael Yurko <myurko@gmail.com>
#
#This file is part of pyQAP.
#
#pyQAP is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 2 of the License, or
#(at your option) any later version.
#
#pyQAP is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with pyQAP.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

import numpy as np
cimport numpy as np
cimport cython
ctypedef np.float64_t float_t
ctypedef Py_ssize_t np_int_t
ctypedef int int_t

@cython.boundscheck(False)
@cython.wraparound(False)
def get_delta(int_t i, int_t j, np.ndarray[np_int_t, ndim=1, mode='c'] sol,\
                        np.ndarray[float_t, ndim=2, mode='c'] f,\
                        np.ndarray[float_t, ndim=2, mode='c'] d, int_t n):
        '''
        Return the change in cost of swapping the given items.
        '''
        cdef double c
        cdef int_t k
        c = (f[j, j]-f[i, i])*(d[sol[i], sol[i]]-d[sol[j], sol[j]])\
            +(f[j, i]-f[i, j])*(d[sol[i], sol[j]]-d[sol[j], sol[i]])
        for k in range(n):
            if k != i and k!=j:
                c += (f[j, k]-f[i, k])*(d[sol[i], sol[k]]-d[sol[j], sol[k]])\
                        +(f[k, j]-f[k, i])*(d[sol[k], sol[i]]-d[sol[k], sol[j]])
        return c
