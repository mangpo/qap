import path_setter
import numpy as np
#import lap
import pyQAP.qap.classes as classes
import pyQAP.qap.heuristics as heuristics
import pyQAP.qap.bound_helpers as bound_helpers

# This is a test of the subproblem reduction. 
def main():
    f=np.array([[0, 1, 1, 0], [1, 0, 2, 3], [0, 2, 0, 5], [0, 2, 5, 0]], dtype=np.float);
    d=np.array([[0, 1, 3, 1], [1, 0, 2, 2], [3, 2, 0, 3], [1, 2, 3, 0]], dtype=np.float);
    #c=np.zeros((4,4))
    c=np.array([[10, 11, 12, 13], [0, 20, 22, 23], [0, 0, 30, 33], [0, 0, 0, 40]], dtype=np.float);    
    problem=classes.Problem(f,d,c);
    
    print problem.f
    print problem.d
    print problem.c

    psol=classes.PSolution(problem.n)

    psol=heuristics.first(problem)
    print psol
    # THE FOLLOWING DEPENDS ON psol BEING the IDENTITY!!!!!!!
    totalcost=psol.cost(problem)
    print 'original cost'
    print totalcost
    print 'Should be 153'

    for i in range(1,problem.n):
        partial=classes.PSolution(problem.n)
        complement=classes.PSolution(problem.n-i)
        for j in range(i):
            partial=partial.add_new(psol.sol[j])
        for j in range(problem.n-i):
            complement=complement.add_new(psol.sol[i+j]-i)

        print 'partial'
        print partial
        print 'complement'
        print complement
        subp=bound_helpers.sub_problem(problem,partial)
        print subp
        subcost=complement.cost(subp)
        print 'ssubcost'
        print subcost
        if subcost != totalcost:
            print "fail"
            1/0
        else:
            print "pass"

    # Now, a more complicated test. Start with [2,3,1,0]
    psol=heuristics.first(problem)
    print problem
    psol.sol[0]=2
    psol.sol[1]=3
    psol.sol[2]=1
    psol.sol[3]=0

    totalcost=psol.cost(problem)
    print 'original cost'
    print totalcost
    partial=classes.PSolution(problem.n)
    complement=classes.PSolution(problem.n-1)
    partial=partial.add_new(2)
    # What is left, appropriately shifted is [2,1,0]
    complement=complement.add_new(2)
    complement=complement.add_new(1)
    complement=complement.add_new(0)
    print 'partial'
    print partial
    print 'complement'
    print complement
    subp=bound_helpers.sub_problem(problem,partial)
    print subp
    subcost=complement.cost(subp)
    print 'ssubcost'
    print subcost
    if subcost != totalcost:
        print "fail"
        1/0
    else:
        print "pass"

    
if __name__ == "__main__":
    main()
