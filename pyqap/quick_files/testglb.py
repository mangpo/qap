import path_setter
import numpy as np
from pyQAP.qap import *
# This is a test of the subproblem reduction. 
def main():
    f=np.array([[0,1,1,3],[1,0,4,4],[1,4,0,5],[3,4,5,0]], dtype=np.float)
    d=np.array([[0,1,3,1],[1,0,4,2],[3,4,0,2],[1,2,2,0]], dtype=np.float)
    c=np.zeros((4,4))
    problem=classes.Problem(f,d,c)
    psol=classes.PSolution(problem.n)
    lap.create_hungarian(problem.n)
    value = bound_helpers.gilmore_lawler_full(problem)
    print value
    if  value != 60:
        print "Error on GLB"
    else:
        print "No error on GLB"
    
if __name__=="__main__":
    main()
