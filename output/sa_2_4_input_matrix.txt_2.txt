Data file name : 
input/2_4_input_matrix.txt
nr iterations, nr resolutions : 
Iteration = 1  Cost = 250  Computational time = 0
Iteration = 3  Cost = 206  Computational time = 0
Iteration = 5  Cost = 202  Computational time = 0
Iteration = 9  Cost = 194  Computational time = 0
Iteration = 14  Cost = 126  Computational time = 0
Best solution found : 
8 1 3 6 4 2 7 5 
Iteration = 1  Cost = 234  Computational time = 0
Iteration = 4  Cost = 232  Computational time = 0
Iteration = 6  Cost = 168  Computational time = 0
Iteration = 17  Cost = 138  Computational time = 0
Iteration = 18  Cost = 126  Computational time = 0
Best solution found : 
7 4 6 2 5 3 1 8 
Iteration = 6  Cost = 138  Computational time = 0
Iteration = 26  Cost = 132  Computational time = 0
Iteration = 30  Cost = 126  Computational time = 0
Best solution found : 
5 7 1 4 8 2 3 6 
