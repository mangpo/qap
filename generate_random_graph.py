from random import *

graph = {}
length = 18
width = 8

def main():
    dim = length*width
    for d in range(dim):
        graph[d] = [];

    for d in range(dim):
        #should add about 1 edge (an incoming and outcoming count)
        #for each node
        #so each node should have degree 2 on avg
        #but we're going to cap at 4
        for i in range(2):
            prob = random();
            if prob > .5:
                other_node = randrange(0,dim)
                if (other_node!=d \
                    and len(graph[d])<4 \
                    and len(graph[other_node])<4 \
                    and not other_node in graph[d] \
                    and not d in graph[other_node] ):
                    #let's go ahead and add the edge
                    graph[d].append(other_node)
                    graph[other_node].append(d)

    print dim, length, width
    for d in range(dim):
        for endpoint in graph[d]:
            print d, endpoint, randrange(1,21) # node1, node2, flow between 1 and 2
            #note that we'll list competing flows, but the transformation to the
            #matrix view will just accept the last one which is fine


main()
