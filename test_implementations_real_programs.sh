#!/bin/bash

for INPUT in "ssd_matrix.txt" "md5_matrix.txt"
do
for IMPL in "sa" "fant" "tabou"
do
echo "$IMPL - $INPUT"
   for I in 1 2 3
   do
        if [ $IMPL == "sa" ]
	  then
	  time ./$IMPL <<< "input/$INPUT 10000000 3" > "output/"$IMPL"_"$INPUT"_"$I".txt"
	fi
        if [ $IMPL == "fant" ]
	  then
	  time ./$IMPL <<< "input/$INPUT 10 2000" > "output/"$IMPL"_"$INPUT"_"$I".txt"
	fi
        if [ $IMPL == "tabou" ]
	  then
	  time ./$IMPL <<< "input/$INPUT" > "output/"$IMPL"_"$INPUT"_"$I".txt"
	fi
   done
echo
done
done
