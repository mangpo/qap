#!/usr/bin/python

import copy, numpy, sys

####################################################################
# Implementation of Kabadi and Punnen's algorithm from
# "An O(n^4) Algorithm for the QAP Linearization Problem" in 2011.
# This program check if a QAP instance is linearizable or not. 
#
# Author: Phitchaya Mangpo Phothilimthana
#
# Date: May, 8th 2013
#
# Input file format:
# n
# (nxn) distance matrix
# (nxn) flow matrix
#
# Example of input file:
# 4
# 0 1 1 2
# 1 0 2 1
# 1 2 0 1
# 2 1 1 0
#
# 0 1 1 2
# 1 0 1 1
# 1 1 0 1
# 2 1 1 0
#
# Usage:
# ./lineariable.py < input.dat
####################################################################

# size of the problem
n = 0

####################################################################
# Procedures for Phase 1: 
# - Convert Q to Q^R (Quadratic Reduced Form)
# - Remove zero and redundant entries to obtian Q_bar
####################################################################

def pass1(q,r,s,p):
  out = copy.deepcopy(q)

  for l in xrange(n):
    if l != s: # l \in N_s
      out[r][s][p][l] = q[r][s][p][l] - q[r][s][p][n-1]

  out[r][s][r][s] = q[r][s][r][s] + q[r][s][p][n-1]
  return out

def pass2(q,r,s,p):
  out = copy.deepcopy(q)

  for k in xrange(n):
    if k != r: # k \in N_r
      out[r][s][k][p] = q[r][s][k][p] - q[r][s][n-1][p]

  out[r][s][r][s] = q[r][s][r][s] + q[r][s][n-1][p]
  return out

# Check post-condition that w_rsnp = 0 after pass1 of admissible transformations
def check_rsnp(q,r,s):
  for p in xrange(n):
    if(p != r):
      # TYPO in the paper!!!!!
      # if(q[r][s][n-1][p] != 0): #org
      #   print "FAILED AT r =", r, "s =", s, "n =", n-1, "p =", p
      #   assert(q[r][s][n-1][p] == 0)
      if(q[r][s][p][n-1] != 0):
        print "FAILED AT r =", r, "s =", s, "p =", p, "n-1 =", n
        assert(q[r][s][p][n-1] == 0)

# Check post-condition that w_rsnp = 0 after pass2 of admissible transformations
def check_zero(q,r,s):
  for p in xrange(n):
    if q[r][s][n-1][p] !=0 and (r != n-1 or s != p):
      print "FAILED AT r =", r, "s =", s, "n =", n-1, "p =", p
      assert(q[r][s][n-1][p] == 0 or (r == n-1 and s == p))

    if q[r][s][p][n-1] !=0 and (r != p or s != n-1):
      print "FAILED AT r =", r, "s =", s,  "p =", p, "n =", n-1
      assert(q[r][s][p][n-1] == 0 or (r == p and s == n-1))

# Algorithm 2 in the paper
def row_reduce(q):
  for r in xrange(n):
    for s in xrange(n):
      for p in xrange(n):
        if p != r:
          q = pass1(q,r,s,p)

      check_rsnp(q,r,s)
        
      for p in xrange(n):
        if p != s:
          q = pass2(q,r,s,p)

      check_zero(q,r,s)

  return q

# Convert D and F matrix into quadratic Q matrix
def quadratic(d,f):
  q = numpy.zeros((n,n,n,n))
  for i in xrange(n):
    for j in xrange(n):
      for k in xrange(n):
        for l in xrange(n):
          q[i][j][k][l] = d[i][k] * f[j][l]
  return q

# Transpose function on quadratic matrix
def transpose(q):
  out = copy.deepcopy(q)
  for i in xrange(n):
    for j in xrange(n):
      for k in xrange(n):
        for l in xrange(n):
          out[i][j][k][l] = q[k][l][i][j]
  return out

# Check if a given matrix is in quadratic reduced form
def zero_redundant(q):
  for p in xrange(n):
    for i in xrange(n):
      for j in xrange(n):
        assert(q[n-1][p][i][j] == 0 or (i == n-1 and j == p))
        assert(q[p][n-1][i][j] == 0 or (i == p and j == n-1))
        assert(q[i][j][n-1][p] == 0 or (i == n-1 and j == p))
        assert(q[i][j][p][n-1] == 0 or (i == p and j == n-1))

# Remove redundant rows and columns
def remove_redundant(q):
  out = numpy.zeros((n-1,n-1,n-1,n-1))
  for i in xrange(n-1):
    for j in xrange(n-1):
      for k in xrange(n-1):
        for l in xrange(n-1):
          out[i][j][k][l] = q[i][j][k][l]
  return out

# Reduce M^n^2 to M^(n-1)^2
def reduce(d,f):
  n = len(d)
  q = quadratic(d,f)

  # Reduce Q to Q^R quadratic reduced form
  q = row_reduce(q)
  q = transpose(q)
  q = row_reduce(q)

  # Check if Q^R is indeed in quadratic reduced form
  zero_redundant(q)

  # Remove redundant rows and columns
  q = remove_redundant(q)

  return q

####################################################################
# Procedures for Phase 2: 
# - Check if the two conditions hold
####################################################################

# Set diagonal entries of Q_bar to zero => Q'
def set_diag_zero(q_bar):
  out = copy.copy(q_bar)
  for i in xrange(n-1):
    for j in xrange(n-1):
      out[i][j][i][j] = 0
  return out

# Return True if Q' is QP-constant
def qp_constant(q):
  sol = [None]
  vis = [0 for i in xrange(n-1)]
  perm = []

  # Return Q[\pi]
  def evaluate():
    sum = 0
    for i in xrange(n-1):
      for k in xrange(n-1):
        j = perm[i]
        l = perm[k]
        sum = sum + q[i][j][k][l]
    return sum
    
  # Gernate permutatoin and calculate Q[\pi]
  def inner():
    if len(perm) == n-1:
      res = evaluate()
      if sol[0] == None:
        sol[0] = res
        return True
      else:
        return sol[0] == res

    for i in xrange(n-1):
      if not vis[i]:
        vis[i] = 1
        perm.append(i)
        ret = inner()
        if not ret:
          return False
        vis[i] = 0
        del perm[len(perm)-1]

    return True

  return inner()

# Return True if Zij is LP-constant
def lp_constant(z):
  size = len(z)
  sol = [None]
  vis = [0 for i in xrange(size)]
  perm = []
  
  def evaluate():
    sum = 0
    for i in xrange(size):
      j = perm[i]
      sum = sum + z[i][j]
    return sum

  # Gernate permutatoin and calculate z[\pi]
  def inner():
    if len(perm) == size:
      res = evaluate()
      if sol[0] == None:
        sol[0] = res
        return True
      else:
        return sol[0] == res

    for i in xrange(size):
      if not vis[i]:
        vis[i] = 1
        perm.append(i)
        ret = inner()
        if not ret:
          return False
        vis[i] = 0
        del perm[len(perm)-1]

    return True
  return inner()

# Return Zij with row i and column j deleted
def zij(q_bar,i,j):
  # Construct Zij
  z = numpy.zeros((n-1,n-1))
  for u in xrange(n-1):
    for v in xrange(n-1):
      if (i == u) and (j == v):
        z[u][v] = q_bar[i][j][u][v]
      else:
        z[u][v] = q_bar[i][j][u][v] + q_bar[u][v][i][j]

  # Delete row i and column j
  z_del = []
  for u in xrange(n-1):
    if u != i:
      row = []
      for v in xrange(n-1):
        if v != j:
          row.append(z[u][v])
      z_del.append(row)
  return z_del

# Return true if Q_bar is linearizable
def linearizable(q_bar):
  # Condition 1
  for i in xrange(n-1):
    for j in xrange(n-1):
      z = zij(q_bar,i,j)
      if not lp_constant(z):
        print "Z^ij is not LP-constant at i =", i, ", j =", j, "(index starts from 0)"
        return False

  # Condition 2
  q_prime = set_diag_zero(q_bar)
  answer = qp_constant(q_prime)
  if not answer:
    print "Q' is not QP-constant"
  return answer

if __name__ == "__main__":
  inp = sys.stdin

  # N
  n = int(inp.readline())

  # Distance
  i = 0
  d = []
  while i < n:
    s = inp.readline().split()
    if len(s) == n:
      d.append([int(x) for x in s])
      i = i + 1
    elif len(s) != 0:
      raise "Input matrix size doesn't match given dimension."

  # Flow
  i = 0
  f = []
  while i < n:
    s = inp.readline().split()
    if len(s) == n:
      f.append([int(x) for x in s])
      i = i + 1
    elif len(s) != 0:
      raise "Input matrix size doesn't match given dimension."

  # Phase 1: reduce to Q_bar
  q_bar = reduce(d,f)

  # Phase 2: check if linearizable
  print linearizable(q_bar)
