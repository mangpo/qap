\documentclass[10pt,twocolumn]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{fullpage}
\usepackage{enumerate}
\usepackage{cases}
\usepackage[mathscr]{euscript}
\usepackage{hyperref}
\usepackage[margin=.9in]{geometry}

\title{An Application of QAP Solutions to Code Layout on Multicore Processors}

\begin{document}

\maketitle{}

\section{Introduction}

This paper examines the relationship between the Quadratic Assignment Problem (QAP) and the problem of assigning communicating code partitions to cores on many-core processors.  We frame the \emph{partition location} problem as an instance of QAP and explore whether existing techniques from QAP research can be applied to the problem of partition location. 

\subsection{Motivation}

As energy usage has become a first-class concern in architecture design, simpler processor implementations are becoming increasingly necessary, with more energy given over to computation, and less energy expended for processor control.  Simplicity is already the norm in low-power systems. The GreenArrays processor is a recent example of an extremely low-power multicore processor. Each of its 144 cores has less than 300 bytes of memory.  Using such a processor is challenging, because the programmer must split normal programs into small communicating partitions.  The onus is on the programmer to reason about partitioning code, map partitions to cores, and insert communication code. These tasks are difficult and require time and ingenuity. Therefore, we wish to develop a more effective programming framework for such devices, a framework that can perform these partitioning and partition location tasks automatically, allowing programmers to write efficient programs with reasonable amounts of effort and time.

In this project, we focus on how to map program partitions to cores. We refer to this problem as the \emph{partition location} problem.

\subsection{QAP}

The Quadratic Assignment Problem is an extremely difficult optimization problem.  Although some special cases can be solved in polynomial time, the problem is NP-hard in the general case.  It is in fact a generalization of the Traveling Salesman Problem.  The problem is defined as follows:

Given a set $F$ of facilities, a set $L$ of locations, a traffic function $t : F \times F \rightarrow \mathbb{R} $, and a distance function $d : L \times L \rightarrow \mathbb{R} $, find the assignment $a: F \rightarrow L$ that minimizes the following cost function:
      
      $\displaystyle\sum\limits_{f_1 \in F, f_2 \in F} t(f_1,f_2) \cdot d(a(f_1),a(f_2))$  
      
Intuitively, a pair of facilities should be close if there is a large amount of traffic (flow) between them.

\subsection{Partition Location as QAP}

Our partition location problem can be viewed as an instance of the Quadratic Assignment Problem.  Code partitions can be viewed as facilities, and the number of messages sent between each pair of code partitions is the flow between partitions. The distance matrix can be constructed from the shortest distance -- the Manhattan distance -- between each pair of cores in the grid of cores.  (Note that each unit of distance corresponds to one port read.)  The solution to the QAP problem on these graphs is the assignment of partitions to cores that minimizes the total number of port reads during program execution.

Let $d_{ik}$ be distance between location $i$ and $k$, $f_{jl}$ be flow between node $j$ and $l$, N = $\{1, 2, ..., n\}$, and $\mathscr{P}_n$ be the family of all permutations of $N$. Our problem can be framed as a Quadratic Assignment Problem of the following form.
\begin{align*}
\min \sum_{i \in N} \sum_{k \in N_i} d_{ik}f_{\pi(i)\pi(k)} \\
s.t. \quad \pi \in \mathscr{P}_n
\end{align*}
where $N_i = N \backslash \{i\}$.

Alternatively, the cost function can be represented as a $n^2 \times n^2$ matrix $Q = (q_{ijkl})$, where rows are indexed by $(i,j) \in N \times N$ and columns are indexed by $(k,l) \in N \times N$.

\begin{align*}
\min \sum_{i \in N} \sum_{k \in N_i} q_{i \pi(i) k \pi(k)} \\
s.t. \quad \pi \in \mathscr{P}_n
\end{align*}

$Q$ can be computed from $D$ and $F$ such that $q_{ijkl} = d_{ik} \times f_{jl}$.

\section{Approaches}

To solve our partition location problem, we consider the most common techniques for solving QAP.

\subsection{Linearization Approach}

QAP is known to be NP-hard, but some special cases of QAP can be posed as linear assignment problems (LAP) and, thus, can be solved in polynomial time. Kabadi and Punnen demonstrate that an instance of QAP is linearizable if and only if some easily checkable conditions hold.  They also present an algorithm to transform linearizable QAP into LAP and an $O(n^4)$ algorithm to solve the corresponding linearizable problem \cite{Punnen}.  We use the technique invented by Kabadi and Punnen to determine whether all instances of our partition location problem can be linearized. 

For the following section, $\mathbb{M}^n$ is the vector space of all $n \times n$ matrices, and $\mathbb{M}^{n^2}$ is the vector space of all $n^2 \times n^2$ matrices.


\textbf{Theorem 1} For any $Q \in \mathbb{M}^{n^2}$ there exists a matrix $Q^R \in \mathbb{M}^{n^2}$ such that $Q^R$ is in quadratic reduced form, and QP-equivalent to Q.

According to the paper, we can apply \emph{admissible transformation} to $Q$ repeatedly until we obtain $Q^R$. The complexity of the reduction algorithm is $O(n^4)$.  In term of correctness, Burkard et al. \cite{Burkard:textbook} showed that admissible transformation preserves QP-equivalency of the matrix; hence, the result matrix is QP-equivalent to the original one.

Once we obtain $Q^R$, delete all row $(i,j)$ and column $(k,l)$ of $Q^R$ where $(i,j), (k,l) \in \{(n,p), (p,n): p \in N\}$ to get matrix $\bar{Q} \in \mathbb{M}^{(n-1)^2}$. For any $i,j \in N_n$, let $Z^{ij}$ be defined as

\[
z_{uv}^{ij} =
  \begin{cases}
    \bar{q}_{ijuv} & \text{if } (i,j) = (u,v),\\
    \bar{q}_{ijuv} + \bar{q}_{uvij} & \text{if } (i,j) \neq (u,v).
  \end{cases}
\]

Let $Q' \in \mathbb{M}^{(n-1)^2}$ be the matrix obtained from $\bar{Q}$ by setting its diagonal elements to zero ($q'_{ijij} = 0$).

\textbf{Theorem 2} Let $Q^R \in \mathbb{M}^{n^2}$ be in quadratic reduced form. Then $Q^R$ is linearizable if and only if the following two conditions hold:
\begin{enumerate}[(i)]
\item For all $(i,j) \in N_n \times N_n$, the submatrix of $Z^{ij}$ obtained by deleting its $i^{th}$ row and $j^{th}$ column is an LP-constant matrix.
\item $Q'$ is a QP-constant matrix.
\end{enumerate}

For definitions of LP-constant and QP-constant, and the algorithm to reduce $Q$ into its quadratic reduced form $Q^R$, see the Punnen paper \cite{Punnen}.

Using the Punnen approach, we prove that not all instances of our partition location problem are linearizable. A simple counterexample for a $2 \times 2$ grid processor is a program with a complete data flow graph, with edge weight 1 on all edges, save for one edge with weight 2. The counterexample can be represented as $D$ (the distance matrix) and $F$ (the data flow matrix) shown below. 

\begin{center}
\begin{tabular}{cc}
$D = \left| \begin{array}{cccc}
0 & 1 & 1 & 2\\
1 & 0 & 2 & 1\\
1 & 2 & 0 & 1\\
2 & 1 & 1 & 0 \end{array} \right|$
&
$F = \left| \begin{array}{cccc}
0 & 1 & 1 & 2\\
1 & 0 & 1 & 1\\
1 & 1 & 0 & 1\\
2 & 1 & 1 & 0 \end{array} \right|$
\end{tabular}
\end{center}

This instance fails both (i) the LP-constant condition and (ii) the QP-constant condition.  Thus, the partition location problem is not linearizable.

We implemented a program to check if a QAP instance is linearizable, and the code is available at \url{http://www.eecs.berkeley.edu/~mangpo/linearizable.py}.

\subsection{Branch and Bound}

Since the instances of QAP that are of interest to us are not linearizable, we turn to another exact technique. There are three common methods for finding optimal QAP solutions: dynamic programming, cutting plane techniques, and branch and bound. Research has shown that branch and bound is the most successful of the three.

Branch and bound is a \emph{divide and conquer} technique for combinatorial problems. It repeatedly divides a problem into simpler subproblems of the same type, which are then solved or further subdivided. The algorithm generates a search tree with the root corresponding to the original problem.  At each node in the search tree, a relaxed version of the subproblem is solved, typically by loosening the constraints of the subproblem. The solution to the relaxation is a lower bound on the original subproblem. If a lower bound at a given node exceeds the value of a previously known solution, searching on this path cannot lead to an improved solution.

The effectiveness of a branch and bound algorithm depends on:

\begin{enumerate}
\item The strength of the bound
\item How efficiently bounds can be computed
\item The branching decisions
\item The quality of the initial solution
\end{enumerate}

\subsubsection{Lower Bounds}

When we relax constraints on a QAP instance, solving the relaxation produces a lower bound to the optimal solution. There has been substantial work \cite{Li94lowerbounds} on obtaining lower bounds for QAP, using a variety of approaches. The most popular of these is the Gilmore-Lawler bound \cite{LawlerOriginal}, an efficiently computable bound that produces a Linear Assignment Problem (LAP) relaxation of the QAP and then solves it to obtain the lower bound. Other approaches for computing lower bounds may use duals or eigenvalues \footnote{ \url{http://www.seas.upenn.edu/qaplib/lowerbound.html}}. For QAP instances with special classes of distance matrix, there are other specialized bounds \cite{Mittelman08}.

\subsubsection{Branching}
Multiple forms of branching have been proposed in the literature.

\begin{enumerate}
\item \textit{Single assignment} branching: Each problem is divided into subproblems by assigning a location to one of the unassigned facilities.
\item \textit{Pair assignment} branching: Each problem is divided by assigning a pair of facilities to a pair of locations.
\item \textit{Relative positioning} branching: The fixed assignments within each subproblem are expressed in terms of distances between facilities.  Here, the levels of the search tree do not correspond to the number of placed facilities. 
\end{enumerate}

Other decisions also factor into the branching technique, such as which facility is assigned at a branching step, or which node of the search tree to branch first. The appropriate technique is heavily dependent on the type of bound used.

\subsubsection{Heuristics for Initial Solution}
The closer the initial solution is to the optimal, the higher the chance it will be less than some lower bound and cause more branches to be pruned. There are many possible ways to initialize the branch and bound procedure for QAP. We describe results using different heuristics in Section \ref{subsub:emp_bnb}.

\subsubsection{Empirical Evaluation}
\label{subsub:emp_bnb}
To evaluate the performance and behavior of different bounds and heuristics, we use Michael Yurko's branch and bound implementation \footnote{\url{https://code.google.com/p/pyqap/}}.

We first evaluated the runtimes and number of nodes explored while using different lower bounds and a naive initial heuristic. We used QAP instances ranging from size 6 to size 12. Results are summarized in Table \ref{table:time_bnb} and Table \ref{table:nodes_bnb}.  The naive bound is a simple lower bound, which takes the minimum of each individual possible assignments at a given stage.

\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|l|l|l|} \hline
          & None    & Naive   & GL     \\ \hline
    6  & 0.01  & 0.10   & 0.01      \\ \hline
    7  & 0.04  & 0.48   & 0.01     \\ \hline
    8 & 0.32 & 7.12 & 0.15 \\ \hline
    9 & 2.96 & 49.07 & 0.37 \\ \hline
    10 & 32.45 & 343.70 & 0.98 \\ \hline
    11 & 363.70 & Timeout & 4.40 \\ \hline
    12 & Timeout & Timeout & 11.15 \\ \hline
    \end{tabular}
\end{center}
\caption{Mean running time (seconds) for Branch and Bound for three different lower bounds using a naive initial heuristic. GL is the Gilmore-Lawler bound. Timeout=10m.}
\label{table:time_bnb}
\end{table}

\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|l|l|l|} \hline
          & None    & Naive   & GL     \\ \hline
    6  & 1958  & 1335 & 82      \\ \hline
    7  & 13701 & 3827 & 98 \\ \hline
    8 & 109602 & 60151 & 1075 \\ \hline
    9 & 986411 & 292007 & 2240 \\ \hline
    10 & 9864102 & 1429074 & 5534 \\ \hline
    11 & 108505113 & Timeout & 21821 \\ \hline
    12 & Timeout & Timeout & 49228 \\ \hline
    \end{tabular}
\end{center}
\caption{Nodes explored by Branch and Bound for three different lower bounds using a naive initial heuristic. GL is the Gilmore-Lawler bound. Timeout=10m.}
\label{table:nodes_bnb}
\end{table}

In Table \ref{table:nodes_bnb} we note that using a bound causes the algorithm to visit fewer nodes. However, Table \ref{table:time_bnb} reveals that bound computation overhead is an important factor; using no bound is still faster than using the naive bound. Note that Gilmore-Lawler significantly outperforms the others in both time and number of nodes explored.

Using only the Gilmore-Lawler bound, we observed the effects of initial value heuristics on the performance of the algorithm. For problems of size 12 and 14, we used several different initial heuristics. The results\footnote{The specific heuristics are NG=Naive Greedy, GP=Gradient Projection, RGP=Random Gradient Projection, SA=Simulated Annealing, PA=Parallel Annealing. We note that these heuristics produce diverse initial values for the problem. In general, Simulated Annealing and Parallel Annealing produce the best initial values.} are summarized in Tables \ref{table:initial_heuristics_12} and \ref{table:initial_heuristics_14}.

\begin{table}[h!]
\begin{center}
    \begin{tabular}{|c|c|c|c|} \hline
          & Nodes explored    & Time taken (s)     \\ \hline
    NG  & 49228  & 10.89      \\ \hline
    GP  & 49229 & 10.95 \\ \hline
    RGP & 49228 & 10.93 \\ \hline
    SA & 47504 & 10.62 \\ \hline
    PA & 46221 & 10.37 \\ \hline
    \end{tabular}
\end{center}
\caption{Performance with different initial heuristics using the Gilmore-Lawler bound, for a problem of size 12.}
\label{table:initial_heuristics_12}
\end{table}

\begin{table}[h!]
\begin{center}
    \begin{tabular}{|c|c|c|c|} \hline
          & Nodes explored    & Time taken (s)    \\ \hline
    NG  & 970158  & 265.33      \\ \hline
    GP  & 970158 & 286.23 \\ \hline
    RGP & 968254 & 258.99 \\ \hline
    SA & 966958 & 263.94 \\ \hline
    PA & 678375 & 185.60 \\ \hline
    \end{tabular}
\end{center}
\caption{Performance with different initial heuristics using the Gilmore-Lawler bound, for a problem of size 14.}
\label{table:initial_heuristics_14}
\end{table}

For problems of these sizes, it appears initial heuristics have little effect on the overall performance of the algorithm. We do not explore whether this remains true for larger instances. We also note that the Gilmore-Lawler bound grows less effective as problem size increases.

\subsection{QAP Approximations}

Although several special cases of the QAP have been found that are solvable in polynomial time, the general case of QAP remains an extremely difficult optimization problem.  In fact, no exact approach is able to solve problems with size greater than $n=20$ in reasonable time \cite{BurkardQAP}.  With 144 cores in our multicore architecture, we need a solution that scales to $n=144$.

Therefore in addition to the exact approaches described above, we looked at the performance of approximation algorithms on instances of our partition location problem.  Specifically we considered three of the most commonly used QAP approximations: simulated annealing, ant systems, and tabu search.

\subsubsection{Simulated Annealing}

Simulated annealing is a local search method which involves transitioning from a node (a solution) to neighboring nodes (similar solutions).  A temperature schedule controls the extent to which - at any given step of the search - it values exploration relative to exploiting any already discovered information about solution quality.  Improvements in cost are always accepted, while worsening of the cost is accepted dependent on the size of the change and the current temperature.  The temperature, which simulates the role of temperature in cooling physical solids, causing more or less motion, is reduced over the course of the search to shift the focus from exploration to exploitation \cite{Connolly}.

Simulated annealing was first adapted for the QAP by Burkard et al. \cite{Burkard}.  For the QAP, a solution's neighbors are typically defined by the solutions that can be obtained by swapping two facilities' locations \cite{Connolly}.  The starting solution is typically a random assignment of facilities to locations.  Some approaches vary the temperature schedule, tailoring the schedule to QAP problems and producing better results specifically for QAP \cite{Misevicius}.  Others vary the order in which neighbors are picked \cite{Connolly}.

\subsubsection{Ant Systems}

Ant systems are modeled on the behavior of ant colonies, as they seek food and return to the nest, initially searching randomly and later leaving a trail whose strength is proportional to the quantity of the food at the end of the trail.  When more ants reach this same source, the trail of information will be stronger.  These observed patterns are adapted to a search algorithm by treating the ants' search space as analogous to the solution space of a search problem, the amount of food as analogous to the quality of the solution, and the ants' trails as analogous to messages between multiple searchers.  Artificial ants produce solutions and exchange information about it in accordance with a communication scheme that resembles real ant colonies' communication patterns.  Each artificial ant moves from node to a neighboring node, visiting only nodes that it has not seen before.  The choice of next node is influenced by information from the other artificial ants \cite{Dorigo}.

In the context of solving QAP problems, ant-to-ant communication concerns the desirability of pairing a particular facility (of the $n$ facilities) with a particular location (of the $n$ locations).  When a facility-location pairing appears in low cost solutions, its score is better.  For different QAP solutions, this ant-to-ant information has been used either to build wholly new solutions or to modify existing solutions, in the tradition of local search \cite{Gambardella}.

\subsubsection{Tabu Search}

Tabu search, first applied to QAP by Skorin-Kapov \cite{Skorin-Kapov1} \cite{Skorin-Kapov2}, is another local search method.  Tabu search is organized around the idea of a set of available moves from a given solution to a different, similar solution.  Tabu search selects the best neighbor at each stage until there are no neighbors better than the current node - in this way it identifies local optima.  In order to continue search after finding such an optimum (and thereby avoid finding a local optimum rather than the global optimum), the search must be allowed to select worse neighbors.  Once at these worse neighbors, search must be prevented from returning to the local optimum, and so a representation of the move to that optimum is entered into a \emph{tabu list}.  The elements of the tabu (taboo) list represent forbidden moves, which can only be taken if they are deemed sufficiently interesting by an \emph{aspiration criterion}.

As in simulated annealing approaches, the neighborhood of a solution in the QAP problem is typically defined by the set of solutions that can be reached by swapping the locations of a single pair of facilities.  Evaluation of the quality of available moves has been approached in different ways, and may be exact \cite{Fiecther} \cite{Taillard90} or approximate \cite{TaillardRobust}.  The tabu list is typically constituted of pairs of facilities which may not be swapped.  Skorin-Kapov forbade swapping items that had been swapped in the previous $s$ steps, where $s$ is the maximum allowed size of the tabu list \cite{Skorin-Kapov1}.  Taillard suggests forbidding moves that assign both items to locations they have occupied in the previous $s$ steps \cite{TaillardRobust}.

\subsubsection{Empirical Evaluation}

To evaluate the relative performance of these approximate approaches on instances of our partition location problem, we used E. Taillard's C++ implementations of simulated annealing \footnote{\url{http://mistic.heig-vd.ch/taillard/codes.dir/sa_qap.cpp}}, an ant systems variation \footnote{\url{http://mistic.heig-vd.ch/taillard/codes.dir/fant_qap.cpp}}, and a tabu search variation \footnote{\url{http://mistic.heig-vd.ch/taillard/codes.dir/tabou_qap.cpp}}, all of which are specialized for solving QAP problems.  The simulated annealing implementation is modeled on the approach presented by Connolly \cite{Connolly}, the ant system variation is based on the FANT (Fast Ant System) approach by Taillard \cite{TaillardFANT}, and the tabu search is based on Taillard's robust tabu search \cite{TaillardRobust}.

For three different grid sizes, $2 \times 4$, $4 \times 9$, and $8 \times 18$,  we generated a random data flow graph with degree less than or equal to 4 for each node.  For the simulated annealing tests, we used 10,000,000 iterations, and 2 restarts.  For the FANT tests, we used $R=10$ and 2,000 iterations.  As shown in Table \ref{table:time} which displays the running time and Table \ref{table:cost} which displays the solution cost, stimulated annealing produces the best answer in the least amount of time for the instance on the $8 \times 18$ grid, which is the grid size of our multicore processor.

\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|l|l|l|} \hline
    Time (s)      & $2 \times 4$    & $4 \times 9$   & $8 \times 18$     \\ \hline
    SA  & 4.924  & 13.215   & 52.439      \\ \hline
    FANT  & .041  & 1.917   & 156.515     \\ \hline
    Tabu & 2.599 & 66.970 & 1163.683 \\ \hline
    \end{tabular}
\end{center}
\caption{The mean running time (in seconds) over three trials for the three approximation algorithms, on partition location problems ranging from a size of 8 partitions to a size of 144 partitions.}
\label{table:time}
\end{table}


\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|l|l|l|} \hline
    Cost     & $2 \times 4$ & $4 \times 9$ & $8 \times 18$ \\ \hline
    SA    & 126   & 578   & 3306   \\ \hline
    FANT  & 126   & 628   & 3744   \\ \hline
    Tabu & 126   & 588   & 3768   \\ \hline
    \end{tabular}
\end{center}
\caption{The cost of the best solution found by the three approximation algorithms, on partition location problems ranging from a size of 8 partitions to a size of 144 partitions.}
\label{table:cost}
\end{table}

We repeated the same testing process for the data flow graphs of two real programs, binocular disparity, and MD5, that have been partitioned for the multicore processor.  All three approaches yield the same solution cost, as shown in Table \ref{table:timeReal}, but Table \ref{table:costReal} reveals that simulated annealing is fastest.

\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|c|c|} \hline
    Time (s)      & Binocular Disparity    & MD5      \\ \hline
    SA  & 53.429  & 53.685         \\ \hline
    FANT  & 71.104  & 69.494       \\ \hline
    Tabu & 1173.340 & 1181.029  \\ \hline
    \end{tabular}
\end{center}
\caption{The mean running time (in seconds) over three trials for the three approximation algorithms, on two real partition location problems.}
\label{table:timeReal}
\end{table}


\begin{table}[h!]
\begin{center}
    \begin{tabular}{|l|c|c|} \hline
    Cost     & Binocular Disparity    & MD5      \\ \hline
    SA    & 198   & 1668   \\ \hline
    FANT  & 198   & 1668  \\ \hline
    Tabu & 198   & 1668  \\ \hline
    \end{tabular}
\end{center}
\caption{The cost of the best solution found by the three approximation algorithms, on two real partition location problems.}
\label{table:costReal}
\end{table}

\newpage

\bibliographystyle{plain}
\bibliography{report}
\end{document}
